<?php

namespace Drupal\healthchecker\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure health settings for this site.
 */
class HealthcheckerSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'healthchecker_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'healthchecker.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('healthchecker.settings');
    $form['timestamp'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include Timestamp'),
      '#description' => $this->t('If this box is checked the current timestamp will be appended to the health checker response.'),
      '#default_value' => $config->get('timestamp'),
    ];

    $form['endpoint'] = [
      '#title' => $this->t('Healthcheck Endpoint'),
      '#type' => 'textfield',
      '#description' => $this->t("Enter the path for health check page."),
      '#default_value' => $config->get('endpoint'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->isValueEmpty('endpoint')) {
      $form_state->setValueForElement($form['endpoint'], '/healthcheck');
    }

    if (($value = $form_state->getValue('endpoint')) && $value[0] !== '/') {
      $form_state->setErrorByName('endpoint', $this->t("Please ensure that the endpoint begins with a forward slash."));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('healthchecker.settings')
      ->set('timestamp', $form_state->getValue('timestamp'))
      ->set('endpoint', $form_state->getValue('endpoint'))
      ->save();
    \Drupal::service("router.builder")->rebuild();

    parent::submitForm($form, $form_state);
  }

}
