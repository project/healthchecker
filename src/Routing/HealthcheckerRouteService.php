<?php

namespace Drupal\healthchecker\Routing;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Provides the dynamic routes for the health checker module.
 */
class HealthcheckerRouteService implements ContainerInjectionInterface {

  /**
   * The immutable config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   *   ImmutableConfig.
   */
  protected $settings;

  /**
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->settings = $configFactory->get('healthchecker.settings');
  }

  /**
   * Class creator.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function routes() {
    $route_collection = new RouteCollection();

    // Get the endpoint from the config.
    $endpoint = !empty($this->settings->get('endpoint')) ? $this->settings->get('endpoint') : '/healthcheck';

    // Create the health checker endpoint route.
    $route = new Route(
      $endpoint,
      [
        '_controller' => '\Drupal\healthchecker\Controller\HealthcheckerController',
        '_title' => 'Healthchecker',
        '_disable_route_normalizer' => 'TRUE',
      ],
      [
        '_permission'  => 'access healthcheck page',
      ],
      [
        'no_cache'  => 'TRUE',
      ]
    );
    $route_collection->add('healthchecker.content', $route);

    // Create the admin route.
    $route = new Route(
        '/admin/config/development/healthchecker',
        [
          '_form' => '\Drupal\healthchecker\Form\HealthcheckerSettingsForm',
          '_title' => 'Healthchecker Settings',
        ],
        [
          '_permission'  => 'administer healthchecker',
        ]
      );
    $route_collection->add('healthchecker.admin', $route);

    return $route_collection;
  }

}
