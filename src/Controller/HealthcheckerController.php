<?php

namespace Drupal\healthchecker\Controller;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controller for route that returns health check page.
 */
class HealthcheckerController extends ControllerBase {

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $config;

  /**
   * The date/time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a new HealthcheckerController object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The config factory service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The date/time service.
   */
  public function __construct(
    ConfigFactoryInterface $config,
    TimeInterface $time
  ) {
    $this->config = $config;
    $this->time = $time;
  }

  /**
   * Class creator.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('datetime.time')
    );
  }

  /**
   * The healthchecker controller.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Response.
   */
  public function __invoke(): JsonResponse {
    $timestamp = $this->config->get('healthchecker.settings')->get('timestamp') ?? 0;
    $response = ['status' => 'OK'];
    if ($timestamp) {
      $response['timestamp'] = $this->time->getCurrentTime();
    }

    return new JsonResponse($response, 200);
  }

}
